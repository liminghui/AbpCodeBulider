﻿using System;

namespace AbpCodeBuilder.CodeHelper
{
    [Serializable]
    public class ColumnInfo
    {

        public ColumnInfo()
        {
        }

        public string ColumnName { get; set; }
        public string ColumnOrder { get; set; }
        public string DefaultVal { get; set; }
        public string Description { get; set; }
        public bool IsForeignKey { get; set; }
        public bool IsIdentity { get; set; }
        public bool IsPrimaryKey { get; set; }
        public string Length { get; set; }
        public bool Nullable { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 字段的C#数据类型
        /// </summary>
        public string ColumnType { get; set; }
    }
}