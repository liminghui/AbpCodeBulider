﻿using System;
using System.Windows.Forms;
using AbpCodeBuilder.CodeHelper;

namespace AbpCodeBulider.DxUI
{
    public partial class FrmConnection : DevExpress.XtraEditors.XtraForm
    {

        public delegate void ConnectionDb();

        public ConnectionDb ConnectionDelegate;

        public FrmConnection()
        {
            InitializeComponent();
        }

        private void frmConnection_Load(object sender, EventArgs e)
        {
            cboServerType.SelectedIndex = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                switch (cboServerType.SelectedIndex)
                {
                    case 0: //Sql Server
                        if (string.IsNullOrEmpty(txtLoginName.Text))
                        {
                            MessageBox.Show("登录名不能为空！");
                            txtLoginName.Focus();
                            return;
                        }

                        if (string.IsNullOrEmpty(txtPassword.Text))
                        {
                            MessageBox.Show("密码不能为空");
                            txtPassword.Focus();
                            return;
                        }

                        string dataSource = txtServerName.Text;
                        string userId = txtLoginName.Text;
                        string password = txtPassword.Text;



                        string connString =
                            string.Format("Data Source={0};Initial Catalog=master;User ID={1};password={2}",
                                          dataSource, userId, password);
                        string ebConnString = string.Format("server={0};uid={1};pwd={2};database=", dataSource, userId,
                                                            password);
                        //EBconnString = "server=LEO-PMIS;uid=sa;pwd=P@ssw0rd;database=";
                        SqlHelper.ConnString = connString;
                        SqlHelper.EBconnString = ebConnString;
                        if (ConnectionDelegate != null)
                        {
                            ConnectionDelegate();
                            if (ckeSave.Checked)
                            {
                                AbpCodeBulider.DxUI.Properties.Settings.Default.Save();
                            }

                            this.Close();
                        }

                        break;
                }



                
            }
            catch (Exception)
            {
                MessageBox.Show("接连数据库出现错误，请检查登录名或者密码是否正确，再重新连接！");
            }
            

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VisibleControls(bool flag)
        {
            label2.Visible = flag;
            label3.Visible = flag;

            txtLoginName.Visible = flag;
            txtPassword.Visible = flag;
        }

        private void cboServerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cboServerType.SelectedIndex)
            {
                case 0://Sql Server
                    VisibleControls(true);
                    label1.Text = "服务器名称：";
                    txtServerName.Text = ".";

                    break;
                case 1: //Sqlite
                    VisibleControls(false);
                    label1.Text = "链接字符串";
                    txtServerName.Text = "";
                    break;
            }
        }

    }
}
