﻿namespace AbpCodeBulider.DxUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bsiSkins = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblDtText = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lblDbText = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnConncetion = new DevExpress.XtraEditors.SimpleButton();
            this.tlData = new DevExpress.XtraTreeList.TreeList();
            this.tlName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlId = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiSkins,
            this.barSubItem1});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSkins)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "文件";
            this.barSubItem1.Id = 2;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // bsiSkins
            // 
            this.bsiSkins.Caption = "皮肤";
            this.bsiSkins.Id = 1;
            this.bsiSkins.Name = "bsiSkins";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControlTop.Size = new System.Drawing.Size(2001, 35);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 1250);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControlBottom.Size = new System.Drawing.Size(2001, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 35);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 1215);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(2001, 35);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(5);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 1215);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.lblDtText);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.lblDbText);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.btnConncetion);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 35);
            this.panelControl1.Margin = new System.Windows.Forms.Padding(5);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(2001, 68);
            this.panelControl1.TabIndex = 26;
            // 
            // lblDtText
            // 
            this.lblDtText.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblDtText.Appearance.Options.UseForeColor = true;
            this.lblDtText.Location = new System.Drawing.Point(1073, 20);
            this.lblDtText.Margin = new System.Windows.Forms.Padding(5);
            this.lblDtText.Name = "lblDtText";
            this.lblDtText.Size = new System.Drawing.Size(0, 22);
            this.lblDtText.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(868, 20);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(180, 22);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "当前连接的数据表为：";
            // 
            // lblDbText
            // 
            this.lblDbText.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblDbText.Appearance.Options.UseForeColor = true;
            this.lblDbText.Location = new System.Drawing.Point(385, 20);
            this.lblDbText.Margin = new System.Windows.Forms.Padding(5);
            this.lblDbText.Name = "lblDbText";
            this.lblDbText.Size = new System.Drawing.Size(0, 22);
            this.lblDbText.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(180, 20);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(180, 22);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "当前连接的数据库为：";
            // 
            // btnConncetion
            // 
            this.btnConncetion.Location = new System.Drawing.Point(20, 12);
            this.btnConncetion.Margin = new System.Windows.Forms.Padding(5);
            this.btnConncetion.Name = "btnConncetion";
            this.btnConncetion.Size = new System.Drawing.Size(125, 39);
            this.btnConncetion.TabIndex = 0;
            this.btnConncetion.Text = "连接数据库";
            this.btnConncetion.Click += new System.EventHandler(this.btnConncetion_Click);
            // 
            // tlData
            // 
            this.tlData.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tlData.Appearance.EvenRow.BackColor2 = System.Drawing.Color.Silver;
            this.tlData.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.tlData.Appearance.EvenRow.Options.UseBackColor = true;
            this.tlData.Appearance.FocusedCell.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tlData.Appearance.FocusedCell.Options.UseBackColor = true;
            this.tlData.Appearance.FocusedRow.BackColor = System.Drawing.Color.Silver;
            this.tlData.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.Gray;
            this.tlData.Appearance.FocusedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.tlData.Appearance.FocusedRow.Options.UseBackColor = true;
            this.tlData.Appearance.SelectedRow.BackColor = System.Drawing.Color.Silver;
            this.tlData.Appearance.SelectedRow.BackColor2 = System.Drawing.Color.Gray;
            this.tlData.Appearance.SelectedRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.tlData.Appearance.SelectedRow.Options.UseBackColor = true;
            this.tlData.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlName,
            this.tlId});
            this.tlData.Dock = System.Windows.Forms.DockStyle.Left;
            this.tlData.Location = new System.Drawing.Point(0, 103);
            this.tlData.Margin = new System.Windows.Forms.Padding(0);
            this.tlData.Name = "tlData";
            this.tlData.OptionsBehavior.Editable = false;
            this.tlData.OptionsView.ShowColumns = false;
            this.tlData.OptionsView.ShowHorzLines = false;
            this.tlData.OptionsView.ShowIndicator = false;
            this.tlData.OptionsView.ShowVertLines = false;
            this.tlData.Size = new System.Drawing.Size(308, 1147);
            this.tlData.TabIndex = 27;
            this.tlData.AfterExpand += new DevExpress.XtraTreeList.NodeEventHandler(this.tlData_AfterExpand);
            this.tlData.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlData_FocusedNodeChanged);
            this.tlData.DoubleClick += new System.EventHandler(this.tlData_DoubleClick);
            // 
            // tlName
            // 
            this.tlName.FieldName = "name";
            this.tlName.Name = "tlName";
            this.tlName.OptionsColumn.ShowInCustomizationForm = false;
            this.tlName.Visible = true;
            this.tlName.VisibleIndex = 0;
            // 
            // tlId
            // 
            this.tlId.FieldName = "id";
            this.tlId.Name = "tlId";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(308, 103);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(5);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(8, 1147);
            this.splitterControl1.TabIndex = 32;
            this.splitterControl1.TabStop = false;
            // 
            // documentManager1
            // 
            this.documentManager1.MdiParent = this;
            this.documentManager1.MenuManager = this.barManager1;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2001, 1250);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.tlData);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "FrmMain";
            this.Text = "Abp代码生成平台";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem bsiSkins;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton btnConncetion;
        private DevExpress.XtraTreeList.TreeList tlData;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlId;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblDbText;
        private DevExpress.XtraEditors.LabelControl lblDtText;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;

    }
}

