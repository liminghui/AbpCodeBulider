﻿namespace AbpCodeBulider.DxUI
{
    partial class FrmSlnRename4Abp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.txtSlnDir = new DevExpress.XtraEditors.TextEdit();
            this.btnOpenSlnDir = new DevExpress.XtraEditors.SimpleButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOldSlnName = new DevExpress.XtraEditors.TextEdit();
            this.txtOldCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOldProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNewCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNewSlnName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.rec = new DevExpress.XtraRichEdit.RichEditControl();
            this.btnRename = new DevExpress.XtraEditors.SimpleButton();
            this.btnRead = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtSlnDir.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldSlnName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewSlnName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(267, 22);
            this.label4.TabIndex = 1;
            this.label4.Text = "请输入解决方案目录(含有.sln) : ";
            // 
            // txtSlnDir
            // 
            this.txtSlnDir.EditValue = "";
            this.txtSlnDir.Location = new System.Drawing.Point(303, 46);
            this.txtSlnDir.Margin = new System.Windows.Forms.Padding(5);
            this.txtSlnDir.Name = "txtSlnDir";
            this.txtSlnDir.Size = new System.Drawing.Size(717, 28);
            this.txtSlnDir.TabIndex = 0;
            this.txtSlnDir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSlnDir_KeyPress);
            // 
            // btnOpenSlnDir
            // 
            this.btnOpenSlnDir.Location = new System.Drawing.Point(1030, 40);
            this.btnOpenSlnDir.Margin = new System.Windows.Forms.Padding(5);
            this.btnOpenSlnDir.Name = "btnOpenSlnDir";
            this.btnOpenSlnDir.Size = new System.Drawing.Size(125, 39);
            this.btnOpenSlnDir.TabIndex = 1;
            this.btnOpenSlnDir.Text = "选择";
            this.btnOpenSlnDir.Click += new System.EventHandler(this.btnOpenSlnDir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 22);
            this.label1.TabIndex = 3;
            this.label1.Text = "源解决方案名 : ";
            // 
            // txtOldSlnName
            // 
            this.txtOldSlnName.EditValue = "";
            this.txtOldSlnName.Location = new System.Drawing.Point(303, 100);
            this.txtOldSlnName.Margin = new System.Windows.Forms.Padding(5);
            this.txtOldSlnName.Name = "txtOldSlnName";
            this.txtOldSlnName.Properties.ReadOnly = true;
            this.txtOldSlnName.Size = new System.Drawing.Size(529, 28);
            this.txtOldSlnName.TabIndex = 3;
            // 
            // txtOldCompanyName
            // 
            this.txtOldCompanyName.EditValue = "";
            this.txtOldCompanyName.Location = new System.Drawing.Point(303, 152);
            this.txtOldCompanyName.Margin = new System.Windows.Forms.Padding(5);
            this.txtOldCompanyName.Name = "txtOldCompanyName";
            this.txtOldCompanyName.Properties.ReadOnly = true;
            this.txtOldCompanyName.Size = new System.Drawing.Size(529, 28);
            this.txtOldCompanyName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(121, 155);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "源解决方案公司名 : ";
            // 
            // txtOldProjectName
            // 
            this.txtOldProjectName.EditValue = "";
            this.txtOldProjectName.Location = new System.Drawing.Point(303, 205);
            this.txtOldProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.txtOldProjectName.Name = "txtOldProjectName";
            this.txtOldProjectName.Properties.ReadOnly = true;
            this.txtOldProjectName.Size = new System.Drawing.Size(529, 28);
            this.txtOldProjectName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 208);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 22);
            this.label3.TabIndex = 7;
            this.label3.Text = "源解决方案项目名 : ";
            // 
            // txtNewCompanyName
            // 
            this.txtNewCompanyName.EditValue = "";
            this.txtNewCompanyName.Location = new System.Drawing.Point(303, 331);
            this.txtNewCompanyName.Margin = new System.Windows.Forms.Padding(5);
            this.txtNewCompanyName.Name = "txtNewCompanyName";
            this.txtNewCompanyName.Size = new System.Drawing.Size(529, 28);
            this.txtNewCompanyName.TabIndex = 7;
            this.txtNewCompanyName.EditValueChanged += new System.EventHandler(this.txtNewCompanyName_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(121, 334);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 22);
            this.label5.TabIndex = 9;
            this.label5.Text = "新解决方案公司名 : ";
            // 
            // txtNewProjectName
            // 
            this.txtNewProjectName.EditValue = "";
            this.txtNewProjectName.Location = new System.Drawing.Point(303, 378);
            this.txtNewProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.txtNewProjectName.Name = "txtNewProjectName";
            this.txtNewProjectName.Size = new System.Drawing.Size(529, 28);
            this.txtNewProjectName.TabIndex = 8;
            this.txtNewProjectName.EditValueChanged += new System.EventHandler(this.txtNewProjectName_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(121, 381);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 22);
            this.label6.TabIndex = 11;
            this.label6.Text = "新解决方案项目名 : ";
            // 
            // txtNewSlnName
            // 
            this.txtNewSlnName.EditValue = "";
            this.txtNewSlnName.Location = new System.Drawing.Point(303, 282);
            this.txtNewSlnName.Margin = new System.Windows.Forms.Padding(5);
            this.txtNewSlnName.Name = "txtNewSlnName";
            this.txtNewSlnName.Properties.ReadOnly = true;
            this.txtNewSlnName.Size = new System.Drawing.Size(529, 28);
            this.txtNewSlnName.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(157, 285);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 22);
            this.label7.TabIndex = 13;
            this.label7.Text = "新解决方案名 : ";
            // 
            // rec
            // 
            this.rec.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.rec.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rec.Appearance.Text.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rec.Appearance.Text.Options.UseFont = true;
            this.rec.Location = new System.Drawing.Point(14, 421);
            this.rec.Margin = new System.Windows.Forms.Padding(5);
            this.rec.Name = "rec";
            this.rec.Options.AutoCorrect.DetectUrls = false;
            this.rec.Options.AutoCorrect.ReplaceTextAsYouType = false;
            this.rec.Options.Behavior.PasteLineBreakSubstitution = DevExpress.XtraRichEdit.LineBreakSubstitute.Paragraph;
            this.rec.Options.DocumentCapabilities.Bookmarks = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.CharacterStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.HeadersFooters = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Hyperlinks = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.InlinePictures = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.Bulleted = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.MultiLevel = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Numbering.Simple = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.ParagraphFormatting = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Paragraphs = DevExpress.XtraRichEdit.DocumentCapability.Enabled;
            this.rec.Options.DocumentCapabilities.ParagraphStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Sections = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.Tables = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.DocumentCapabilities.TableStyle = DevExpress.XtraRichEdit.DocumentCapability.Disabled;
            this.rec.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.rec.Size = new System.Drawing.Size(1473, 430);
            this.rec.TabIndex = 10;
            this.rec.Views.DraftView.AllowDisplayLineNumbers = true;
            this.rec.Views.DraftView.Padding = new System.Windows.Forms.Padding(80, 4, 0, 0);
            this.rec.Views.SimpleView.Padding = new System.Windows.Forms.Padding(50, 4, 4, 0);
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(842, 372);
            this.btnRename.Margin = new System.Windows.Forms.Padding(5);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(125, 39);
            this.btnRename.TabIndex = 9;
            this.btnRename.Text = "确认";
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(1165, 40);
            this.btnRead.Margin = new System.Windows.Forms.Padding(5);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(125, 39);
            this.btnRead.TabIndex = 2;
            this.btnRead.Text = "读取目录";
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // FrmSlnRename4Abp
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1501, 865);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.rec);
            this.Controls.Add(this.txtNewSlnName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNewProjectName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNewCompanyName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtOldProjectName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtOldCompanyName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtOldSlnName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSlnDir);
            this.Controls.Add(this.btnOpenSlnDir);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmSlnRename4Abp";
            this.Text = "解决方案重命名(Abp)";
            this.Load += new System.EventHandler(this.FrmSlnRename4Abp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtSlnDir.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldSlnName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOldProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNewSlnName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtSlnDir;
        private DevExpress.XtraEditors.SimpleButton btnOpenSlnDir;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtOldSlnName;
        private DevExpress.XtraEditors.TextEdit txtOldCompanyName;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtOldProjectName;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtNewCompanyName;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txtNewProjectName;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtNewSlnName;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraRichEdit.RichEditControl rec;
        private DevExpress.XtraEditors.SimpleButton btnRename;
        private DevExpress.XtraEditors.SimpleButton btnRead;
    }
}