﻿namespace AbpCodeBulider.DxUI
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbBox = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSingleGen = new DevExpress.XtraEditors.SimpleButton();
            this.btnSlnRename = new DevExpress.XtraEditors.SimpleButton();
            this.gbBox.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbBox
            // 
            this.gbBox.BackColor = System.Drawing.Color.Transparent;
            this.gbBox.Controls.Add(this.flowLayoutPanel1);
            this.gbBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbBox.Location = new System.Drawing.Point(17, 17);
            this.gbBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbBox.Name = "gbBox";
            this.gbBox.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbBox.Size = new System.Drawing.Size(1497, 273);
            this.gbBox.TabIndex = 1;
            this.gbBox.TabStop = false;
            this.gbBox.Text = "常用操作";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnSingleGen);
            this.flowLayoutPanel1.Controls.Add(this.btnSlnRename);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 27);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1489, 241);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btnSingleGen
            // 
            this.btnSingleGen.Location = new System.Drawing.Point(41, 42);
            this.btnSingleGen.Margin = new System.Windows.Forms.Padding(41, 42, 41, 42);
            this.btnSingleGen.Name = "btnSingleGen";
            this.btnSingleGen.Size = new System.Drawing.Size(230, 39);
            this.btnSingleGen.TabIndex = 0;
            this.btnSingleGen.Text = "单表代码生成器";
            this.btnSingleGen.Click += new System.EventHandler(this.btnSingleGen_Click);
            // 
            // btnSlnRename
            // 
            this.btnSlnRename.Location = new System.Drawing.Point(353, 42);
            this.btnSlnRename.Margin = new System.Windows.Forms.Padding(41, 42, 41, 42);
            this.btnSlnRename.Name = "btnSlnRename";
            this.btnSlnRename.Size = new System.Drawing.Size(230, 39);
            this.btnSlnRename.TabIndex = 1;
            this.btnSlnRename.Text = "解决方案重命名(Abp)";
            this.btnSlnRename.Click += new System.EventHandler(this.btnSlnRename_Click);
            // 
            // FrmMenu
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1531, 768);
            this.Controls.Add(this.gbBox);
            this.LookAndFeel.SkinName = "Office 2010 Blue";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmMenu";
            this.Padding = new System.Windows.Forms.Padding(17);
            this.Text = "  主页  ";
            this.gbBox.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBox;
        private DevExpress.XtraEditors.SimpleButton btnSingleGen;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnSlnRename;
    }
}