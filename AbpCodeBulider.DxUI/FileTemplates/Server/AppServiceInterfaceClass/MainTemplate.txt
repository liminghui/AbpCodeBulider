﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using {{Namespace_Here}}.{{Namespace_Relative_Full_Here}}.Dto;
using {{Namespace_Here}}.Dto;

namespace {{Namespace_Here}}.{{Namespace_Relative_Full_Here}}
{
    public interface I{{Entity_Name_Here}}AppService : IApplicationService 
    {
		Task<PagedResultDto<{{Entity_Name_Here}}Dto>> GetAll(GetAll{{Entity_Name_Plural_Here}}Input input);

		Task<Get{{Entity_Name_Here}}ForEditOutput> Get{{Entity_Name_Here}}ForEdit(EntityDto<{{Primary_Key_Inside_Tag_Here}}> input);

		Task CreateOrEdit(CreateOrEdit{{Entity_Name_Here}}Dto input);

		Task Delete(EntityDto<{{Primary_Key_Inside_Tag_Here}}> input);

		Task BatchDelete(BatchDelete{{Entity_Name_Plural_Here}}Input input);

		{{Get_Excel_Method_Here}}
    }
}