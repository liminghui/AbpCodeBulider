﻿using System;
using System.Windows.Forms;
using AbpCodeBuilder.CodeHelper;

namespace AbpCodeBulider.DxUI
{
    internal partial class FrmMenu : FrmBase
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

        private void btnSingleGen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(DBName))
            {
                MessageBox.Show("请选择一个数据库");
                return;
            }
            if (string.IsNullOrEmpty(DataTableId))
            {
                MessageBox.Show("请选择一个表");
                return;
            }

            _FrmMain.CreateForm(typeof(FrmCode));
        }

        private void btnSlnRename_Click(object sender, EventArgs e)
        {
            _FrmMain.CreateForm(typeof(FrmSlnRename4Abp));
        }
    }
}
