﻿using System;
using System.Windows.Forms;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Services;

namespace AbpCodeBulider.DxUI
{
    internal partial class FrmRec : FrmBase
    {
        public FrmRec()
        {
            InitializeComponent();


            rec.AddService(typeof(ISyntaxHighlightService), new SyntaxHighlightService(rec));
            string str = System.IO.Path.GetTempFileName() + "1.cs";
            rec.SaveDocument(str, DocumentFormat.PlainText);
            rec.LoadDocument(str, DocumentFormat.PlainText);
            System.IO.File.Delete(str);


        }

        private void FrmRec_Load(object sender, EventArgs e)
        {


        }



        #region 设置文档
        public void SetText(string str)
        {
            
            rec.Text = str;

        } 
        #endregion

        #region 刷新界面信息
        public static void UIShowCode(FrmBase thisFrm,string frmName, string modelCode)
        {
             if (thisFrm.InvokeRequired)
            {
                thisFrm.Invoke(new MethodInvoker(() => UIShowCode(thisFrm,frmName, modelCode)));
            }
            else
            {
                FrmRec fx = null;
                foreach (Form frm in thisFrm.FrmMain.MdiChildren)
                {
                    try
                    {
                        FrmRec f = (FrmRec)frm;
                        if (f != null)
                        {
                            if (f.strName == frmName)
                            {
                                fx = f;
                                break;
                            }
                        }
                    }
                    catch
                    { 
                    }
                }
                if (fx == null)
                { 
                    fx = new FrmRec();
                    fx.FrmMain = thisFrm.FrmMain;
                    fx.MdiParent = thisFrm.FrmMain;
                    fx.strName = frmName;
                    fx.Text = frmName;
                }
                fx.SetText(modelCode);
                fx.Show();
                fx.Activate();
            }
        }
        #endregion










    }
}
